package com.galvanize.hello;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(HelloController.class)
public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void sayHello_noArg_sayHello() throws Exception {
        //when localhost:8080/hello I should get "Hello from Spring" from spring returned
        //SETUP

        //EXECUTION
        mockMvc.perform(get("/hello"))
                //ASSERT
                .andExpect(status().isOk())
                .andExpect(content().string("Hello from Spring"));
    }

    @Test
    void sayHello_withName_saysHelloNameFromSpring() throws Exception {
        //SETUP

        //EXECUTION
        mockMvc.perform(get("/hello?name=Rob"))
                //ASSERT
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Rob from Spring"));
    }

    @Test
    void calculateAddition_withSeries_result() throws Exception {
        //SETUP

        //EXECUTION
        mockMvc.perform(get("/calculate?series=1,2,3&parameter=+"))
                //ASSERT
                .andExpect(status().isOk())
                .andExpect(content().string("Result: 6"));
    }

    @Test
    void countVowels_withString_result() throws Exception {
        //SETUP

        //EXECUTION
        mockMvc.perform(get("/countVowels?phrase=Spring Boot Is Pretty Cool!"))
                //ASSERT
                .andExpect(status().isOk())
                .andExpect(content().string("Number of vowels: 7"));

    }

    @Test
    void findAndReplace_withReplacement_result() throws Exception {
        //SETUP

        //EXECUTION
        mockMvc.perform(post("/findAndReplace?find=blue&replacement=green")
                .content("The blue house with a blue car was filled with blue flowers"))
                //ASSERT
                .andExpect(status().isOk())
                .andExpect(content().string("The green house with a green car was filled with green flowers"));
    }

    @Test
    void getEmployee_withID_employeeName() throws Exception {
        //SETUP

        //EXECUTION
        mockMvc.perform(get("/getEmployee/4"))
                //ASSERT
                .andExpect(status().isOk())
                .andExpect(content().string("David Lira"));
    }

//    @Test
//    void sendEmployeeInfo_employeeInfo_result() throws Exception {
//        //SETUP
//
//        //EXECUTION
//        mockMvc.perform(post("/sendEmployeInfo").content("{ \"name\": \"David\"; \"city\": \"Long Beach\"}"))
//                //ASSERT
//                .andExpect(status().isOk())
//                .andExpect(content().string("David from Long Beach"));
//    }

}
