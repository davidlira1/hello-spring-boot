package com.galvanize.hello;

import org.springframework.web.bind.annotation.*;

import static java.lang.Integer.parseInt;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(@RequestParam(defaultValue = "", required = false) String name) {
        return name.equals("") ? "Hello from Spring" : String.format("Hello %s from Spring", name);
    }

    @GetMapping("/calculate")
    public String calculate(@RequestParam() String series, @RequestParam() String parameter) {
        int result = 0;

        String[] seriesArr = series.split(",");
        for(String str : seriesArr) {
            result += parseInt(str);
        }

        return "Result: " + result;
    }

    @GetMapping("/countVowels")
    public String countVowels(@RequestParam() String phrase) {
        int numberOfVowels = 0;
        phrase = phrase.toLowerCase();

        for(int i = 0; i < phrase.length(); i++) {
            if(phrase.charAt(i) == 'a' || phrase.charAt(i) == 'e' || phrase.charAt(i) == 'i' || phrase.charAt(i) == 'o' || phrase.charAt(i) == 'u') {
                numberOfVowels++;
            }
        }

        return "Number of vowels: " + numberOfVowels;
    }

    @PostMapping("/findAndReplace")
    public String findAndReplace(@RequestParam() String find, @RequestParam() String replacement, @RequestBody() String body) {

        String modifiedString = body.replaceAll("blue", "green");

        return modifiedString;
    }

    @GetMapping("/getEmployee/{id}")
    public String getEmployee(@PathVariable int id) {
        String[] employees = {"Brandon Jones", "John Volland", "Nick Sturz", "Cassandra Blanca", "David Lira", "Eric Weisner"};

        return employees[id];
    }

//    @PostMapping("/sendEmployeeInfo")
//    public String sendEmployeeInfo(@RequestBody Employee employee) {
//        return employee.toString();
//    }

}
